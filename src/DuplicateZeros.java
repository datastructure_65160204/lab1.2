import java.util.Scanner;

public class DuplicateZeros {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input: ");
        String input = sc.nextLine();
        String[] elements = input.split("[\s,]+");

        int[] arr = new int[elements.length];
        for (int i = 0; i < elements.length; i++) {
            arr[i] = Integer.parseInt(elements[i]);
        }
        duplicateZeros(arr);

        // แสดงผลลัพธ์
        System.out.print("Output: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }

    public static void duplicateZeros(int[] arr) {
        int n = arr.length;
        int zeros = 0;

        // นับจำนวนตัวเลข 0 ในอาร์เรย์
        for (int num : arr) {
            if (num == 0) {
                zeros++;
            }
        }

        // ทำการขยับตัวเลขตามที่กำหนด          // arr = [1,0,2,3,0,4,5,0] -->  [1,0,0,2,3,0,0,4]
        for (int i = n - 1; i >= 0; i--) {  // i=7 n=8  | i=6    | i=5   | i=4    | i=3    | i=2    | i=1     | i=0
            if (arr[i] == 0) {              //[7]==0    | [6]!=0 | [5]!=0| [4]==0 | [3]!=0 | [2]!=0 | [1]==0  | [0]!=0
                if (i + zeros < n) {        //7+3<8 X            |       | 4+2<8 O|        |        | 1+1<8 O |
                    arr[i + zeros] = 0;     //                           | [6]=0                    | [2]=0
                }
                zeros--;                    //z=2                        | z=1                      | z=0
            }
            if (i + zeros < n) {            //7+2<8 X   | 6+2<8 X| 5+2<8 O| 4+1<8 O| 3+1<8 O| 2+1<8 O| 1+0<8 O| 0+0<8 O
                arr[i + zeros] = arr[i];    //                   | [7]= 4 | [5]= 0 | [4]=3  | [3]= 2 | [1]= 0 | [0]= 1
            }
        }
    }
}